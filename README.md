# Clever Captcha
# Benjamin Cassinat - benftwc[at]gmail[dot]com
#
#
# This Drupal 7 Module will extends reCaptcha to purpose or not a captcha depends of your "Pass Test Score".
#
# In facts, if the website recognize you as a Human, you'll pass without captcha, if the website isn't sure, it ask you to enter a captcha code,  but, if the website is fully sure that you arn't a Human, so a Bot, it will kill your request before execute it.
